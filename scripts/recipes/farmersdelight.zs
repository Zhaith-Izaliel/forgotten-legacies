import crafttweaker.api.recipe.replacement.Replacer;
import crafttweaker.api.recipe.replacement.type.ModsFilteringRule;
import crafttweaker.api.recipe.replacement.type.ManagerFilteringRule;
import crafttweaker.api.recipe.replacement.type.ComponentFilteringRule;
import crafttweaker.api.ingredient.IIngredient;


Replacer
	.create()
	.filter(ManagerFilteringRule.of([
		<recipetype:minecraft:smoking>, 
		<recipetype:minecraft:campfire_cooking>, 
		<recipetype:minecraft:smelting>
		])
	)
	.replace<IIngredient>(<recipecomponent:crafttweaker:input/ingredients>, <item:minecraft:egg>, <tag:items:c:eggs>.asIIngredient())
	.execute();
