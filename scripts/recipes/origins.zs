// Origin Orb
craftingTable.addShaped("orb_of_origins",
	<item:origins:orb_of_origin>,
	[
		[<item:minecraft:echo_shard>,<item:minecraft:diamond>,<item:minecraft:echo_shard>],
		[<item:minecraft:amethyst_shard>,<item:minecraft:ender_pearl>,<item:minecraft:amethyst_shard>],
		[<item:minecraft:echo_shard>,<item:minecraft:diamond>,<item:minecraft:echo_shard>],
	]
);

